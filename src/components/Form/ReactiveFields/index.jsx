import DisplayPhoto from "@Components/Form/ReactiveFields/DisplayPhoto";
import ExpiryDateField from "@Form/ReactiveFields/ExpiryDateField";
import Pregnancy from "@Components/Form/ReactiveFields/Pregnancy";
import Age from "@Components/Form/ReactiveFields/Age";
import GovtId from "@Components/Form/ReactiveFields/GovtId";

export { DisplayPhoto, ExpiryDateField, Pregnancy, Age, GovtId };
